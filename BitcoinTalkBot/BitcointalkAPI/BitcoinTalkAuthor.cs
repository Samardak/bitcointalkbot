﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitcointalkAPI
{
    public class BitcoinTalkAuthor
    {
        public string Link { get; set; }
        public string Name { get; set; } //20
        public string Posts { get; set; } //22
        public string Activity { get; set; } //24
        public string Position { get; set; } //28
        public string DateRegistered { get; set; } //30
        public string LastActive { get; set; } //32
        public string Email { get; set; }
        public string Website { get; set; }
        public bool IsExeption { get; set; }
        public Exception exception { get; set; }

        public BitcoinTalkAuthor(string url)
        {
            try
            {
                var web = new HtmlWeb();
                var doc = web.Load(url);
                var tag = doc.DocumentNode.Descendants("td")?.ToList();

                this.Name = GetText(tag, 20);
                this.Posts = GetText(tag, 22);
                this.Activity = GetText(tag, 24);
                this.Position = GetText(tag, 28);
                this.DateRegistered = GetText(tag, 30);
                this.LastActive = GetText(tag, 32);
                this.Email = GetText(tag, 43);
                this.Website = GetInnerHtml(tag, 45);
                this.Link = url;
            }
            catch (Exception ex)
            {
                IsExeption = true;
                exception = ex;
            }
        }

        public string GetText(List<HtmlNode> nodes, int index)
        {
            try
            {
                return nodes[index]?.InnerText;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string GetInnerHtml(List<HtmlNode> nodes, int index)
        {
            try
            {
                return nodes[index]?.InnerHtml;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
